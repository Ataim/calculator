function addToDisplay(val) {
  document.getElementById("display").value += val;
}

function clearDisplay() {
  document.getElementById("display").value = "";
}

function sqrt() {
  document.getElementById("display").value = Math.sqrt(
    document.getElementById("display").value
  );
}

function calculate() {
  try {
    if (document.getElementById("display").value.substring("/0")) {
      alert("Division par zéro impossible !");
      return;
    }
    document.getElementById("display").value = eval(
      document.getElementById("display").value
    );
  } catch (e) {
    alert("Erreur de calcul !");
  }
}
